package com.vaadin.tutorial.crm.backend.service;

import com.github.javafaker.Faker;
import com.vaadin.tutorial.crm.backend.entity.Company;
import com.vaadin.tutorial.crm.backend.entity.Contact;
import com.vaadin.tutorial.crm.backend.repository.CompanyRepository;
import com.vaadin.tutorial.crm.backend.repository.ContactRepository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ContactService {
    private static final Logger LOGGER = Logger.getLogger(ContactService.class.getName());
    private ContactRepository contactRepository;
    private CompanyRepository companyRepository;

    //constructor dependency injection
    public ContactService(ContactRepository contactRepository,
                          CompanyRepository companyRepository) {
        this.contactRepository = contactRepository;
        this.companyRepository = companyRepository;
    }
    //READ-ALL
    public List<Contact> findAll(String stringFilter) {
        if (stringFilter == null || stringFilter.isEmpty()){
            return contactRepository.findAll();
        } else {
            return contactRepository.search(stringFilter);
        }

    }

    public List<Contact> findAll() {
        return contactRepository.findAll();
    }

    public long count() {
        return contactRepository.count();
    }

    //DELETE
    public void delete(Contact contact) {
        contactRepository.delete(contact);
    }

    //CREATE
    public void save(Contact contact) {
        if (contact == null) {
            LOGGER.log(Level.SEVERE,
                "Contact is null. Are you sure you have connected your form to the application?");
            return;
        }
        contactRepository.save(contact);
    }

    @PostConstruct
    public void populateTestData() {
        Faker f = new Faker();


        if (companyRepository.count() == 0) {
            companyRepository.saveAll(

                    Stream.generate(() -> f.company().name())
                            .map(Company::new)
                            .limit(5)
                            .collect(Collectors.toList()
                            )

            );

        }
        if (contactRepository.count() == 0) {

            List<Company> companies = companyRepository.findAll();
            Random random = new Random();
            contactRepository.saveAll(

                    Stream.generate(() ->
                            {
                                Contact c = new Contact();
                                c.setFirstName(f.name().firstName());
                                c.setLastName(f.name().lastName());
                                c.setCompany(companies.get(random.nextInt(companies.size())));
                                c.setStatus(Contact.Status.values()[random.nextInt(Contact.Status.values().length)]);
                                c.setEmail(f.bothify("????????##@gmail.com"));
                                return c;
                            }


                    ).limit(1000)
                            .collect(Collectors.toList()));

        }
    }
}
